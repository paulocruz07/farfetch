On this project i used an aproach similar to MVC with dto's to pass and receive data through the application.
I tried to build an high performance application, using recycler view instead of view pager, so it can recycler 3 by 3 strike of items.
It was a challenge to put it up all together with the scroll vertical and horizontal issues, but with the time i had to do it, i think it was good enough.

I used some external libraries, such us:

- Glide - to easily populate images into an image view from the url provided by the service (can be find inside the adapters).
- Retrofit - to stabilish the communication with the services provided by marvelApi (the logic is on the network package).
- butterknife - to easily attach the xml elements, turning it much more easy to use on the java file, without having to search by id.
