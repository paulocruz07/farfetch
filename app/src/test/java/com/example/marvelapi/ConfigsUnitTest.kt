package com.example.marvelapi

import com.example.marvelapi.Network.DTO.ItemsDto
import com.example.marvelapi.Utils.Configs
import org.junit.Assert
import org.junit.Test

class ConfigsUnitTest {

    @Test
    fun set_md5_correct() {
        val item = Configs.md5("ola")
        Assert.assertEquals(item.isEmpty(), false)
    }

    @Test
    fun set_timestamp_correct() {
        val item = Configs.getTimeStamp()
        Assert.assertEquals(item.isEmpty(), false)
    }
}

