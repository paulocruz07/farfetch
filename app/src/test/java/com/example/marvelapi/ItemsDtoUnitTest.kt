package com.example.marvelapi

import com.example.marvelapi.Network.DTO.ItemsDto
import org.junit.Assert
import org.junit.Test

class ItemsDtoUnitTest{
    @Test
    fun set_name_correct() {
        val item = ItemsDto("x","y","z")
        Assert.assertEquals(item.name, "y")
    }
    @Test
    fun set_type_correct() {
        val item= ItemsDto("x","y","z")
        Assert.assertEquals(item.type, "x")
    }
    @Test
    fun set_resourceURI_correct() {
        val item = ItemsDto("x","y","z")
        Assert.assertEquals(item.resourceURI, "z")
    }
}
