package com.example.marvelapi.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marvelapi.Network.DTO.SimpleDto;
import com.example.marvelapi.R;

import java.util.ArrayList;

public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.ViewHolder> {

    private ArrayList<SimpleDto> mData;
    private LayoutInflater mInflater;
    private Context mContext;

    public SimpleAdapter(Context context, ArrayList<SimpleDto> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public SimpleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.simple_layout, parent, false);
        return new SimpleAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull SimpleAdapter.ViewHolder holder, int position) {
        holder.view_bottom.setVisibility(View.GONE);
        holder.view_up.setVisibility(View.GONE);
        holder.title_tv.setText(mData.get(position).getTitle());
        holder.description_tv.setText((mData.get(position).getDescription() != null && !mData.get(position).getDescription().isEmpty()) ? mData.get(position).getDescription() : mContext.getResources().getString(R.string.no_description));
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title_tv, description_tv;
        View view_bottom;
        View view_up;

        ViewHolder(View itemView) {
            super(itemView);
            title_tv = itemView.findViewById(R.id.title_tv);
            description_tv = itemView.findViewById(R.id.description_tv);
            view_bottom = itemView.findViewById(R.id.view_bottom);
            view_up = itemView.findViewById(R.id.view_up);
        }

    }
}