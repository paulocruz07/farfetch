package com.example.marvelapi.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvelapi.Interfaces.OnItemClickListener;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.R;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private ArrayList<MarvelHero> mData;
    private LayoutInflater mInflater;
    private Context mContext;
    private OnItemClickListener listener;

    public SearchAdapter(Context context, ArrayList<MarvelHero> data, OnItemClickListener listener) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.listener = listener;
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.simple_layout, parent, false);
        return new SearchAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.ViewHolder holder, int position) {
        holder.description_tv.setText(mData.get(position).getName());
        holder.title_tv.setVisibility(View.GONE);
        holder.description_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(mData.get(position));
            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView description_tv;
        TextView title_tv;
        View view_bottom;
        View view_up;

        ViewHolder(View itemView) {
            super(itemView);
            description_tv = itemView.findViewById(R.id.description_tv);
            title_tv = itemView.findViewById(R.id.title_tv);
            view_bottom = itemView.findViewById(R.id.view_bottom);
            view_up = itemView.findViewById(R.id.view_up);
        }

    }
}
