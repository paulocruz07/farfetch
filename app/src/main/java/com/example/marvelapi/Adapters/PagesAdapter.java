package com.example.marvelapi.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvelapi.DatabaseConnection.MarvelHeroesDbConnection;
import com.example.marvelapi.Interfaces.OnItemClickListener;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.Network.DTO.ResponseMarvelHeroes;
import com.example.marvelapi.Network.Models.CharactersModel;
import com.example.marvelapi.Network.ServicesURL;
import com.example.marvelapi.R;
import com.example.marvelapi.Utils.Configs;

import java.util.ArrayList;


public class PagesAdapter extends RecyclerView.Adapter<PagesAdapter.ViewHolder> implements CharactersModel.CharactersListener {


    private int mNumberPages;
    private int actual_position;
    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerView mActualRecyclerView;
    private ProgressDialog dialog;
    private HeroeListAdapter adapter;
    private OnItemClickListener listener;
    private ArrayList<MarvelHero> favourites_list;

    public PagesAdapter(Context context, int mNumberPages, OnItemClickListener listener, ArrayList<MarvelHero> favourites_list) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mNumberPages = mNumberPages;
        this.listener = listener;
        this.favourites_list = favourites_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.fragment_list_heroes, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        actual_position = position;
        mActualRecyclerView = holder.heroes_list_rv;
        callGetAllMarvelHeroes(position);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mNumberPages;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView heroes_list_rv;

        ViewHolder(View itemView) {
            super(itemView);
            heroes_list_rv = itemView.findViewById(R.id.heroes_list_rv);
        }
    }

    private void callGetAllMarvelHeroes(int pos) {
        dialog = ProgressDialog.show(mContext, "",
                "Loading. Please wait...", true);
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String string_to_be_converted_to_MD5 = ts + Configs.getPrivateKey(mContext) + ServicesURL.API_KEY;
        String MD5_Hash_String = Configs.md5(string_to_be_converted_to_MD5);

        CharactersModel charactersModel = new CharactersModel(this);
        charactersModel.start(ts, MD5_Hash_String, pos * 20, "");
    }

    @Override
    public void onCharactersListSuccess(ResponseMarvelHeroes response) {
        dialog.dismiss();
        if (response.getData() != null && response.getData().getResults() != null && !response.getData().getResults().isEmpty()) {
            mActualRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
            adapter = new HeroeListAdapter(mContext, favourites_list, (ArrayList<MarvelHero>) response.getData().getResults(), listener);
            mActualRecyclerView.setAdapter(adapter);

            if (actual_position == mNumberPages - 1) {
                mNumberPages++;
            }
        }
    }


    @Override
    public void onCharactersListFailed(String errorMessage) {
        dialog.dismiss();
    }

    public void setFavouritesList(ArrayList<MarvelHero> list) {
        this.favourites_list = list;
        adapter.setFavouritesList(favourites_list);
    }
}
