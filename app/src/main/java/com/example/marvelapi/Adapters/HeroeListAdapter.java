package com.example.marvelapi.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marvelapi.Controllers.MainActivity;
import com.example.marvelapi.DatabaseConnection.MarvelHeroesDbConnection;
import com.example.marvelapi.Interfaces.OnItemClickListener;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.R;

import java.util.ArrayList;

public class HeroeListAdapter extends RecyclerView.Adapter<HeroeListAdapter.ViewHolder> {

    private ArrayList<MarvelHero> mData;
    private ArrayList<MarvelHero> favourites_list;
    private LayoutInflater mInflater;
    private Context mContext;
    private OnItemClickListener listener;

    public HeroeListAdapter(Context context, ArrayList<MarvelHero> favourites_list, ArrayList<MarvelHero> data, OnItemClickListener listener) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.favourites_list = favourites_list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_heroes_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.marvel_heroe_name.setText(mData.get(position).getName());
        setFavourites(mData);
        if (mData.get(position).isFavourite()) {
            holder.favourite_image.setImageResource(R.drawable.logo);
        } else {
            holder.favourite_image.setImageResource(R.drawable.logo_black);
        }
        holder.favourite_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.get(position).isFavourite()) {
                    mData.get(position).setFavourite(false);
                    holder.favourite_image.setImageResource(R.drawable.logo_black);
                    MainActivity.saveFavourite(mData.get(position), false);
                } else {
                    mData.get(position).setFavourite(true);
                    holder.favourite_image.setImageResource(R.drawable.logo);
                    MainActivity.saveFavourite(mData.get(position), true);
                }
            }
        });

        Glide.with(mContext).load((mData.get(position).getThumbnail().getPath() + "/standard_amazing." + mData.get(position).getThumbnail().getExtension()).replace("http", "https")).into(holder.myImageView);

        holder.myImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(mData.get(position));
            }
        });
    }

    private void setFavourites(ArrayList<MarvelHero> list) {
        for (MarvelHero marvelHero : list) {
            if (favourites_list.contains(marvelHero)) {
                marvelHero.setFavourite(true);
            } else {
                marvelHero.setFavourite(false);
            }
        }

    }

    public void setFavouritesList(ArrayList<MarvelHero> list) {
        this.favourites_list = list;
        notifyDataSetChanged();
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView myImageView, favourite_image;
        TextView marvel_heroe_name;

        ViewHolder(View itemView) {
            super(itemView);
            myImageView = itemView.findViewById(R.id.heroe_image);
            favourite_image = itemView.findViewById(R.id.favourite_image);
            marvel_heroe_name = itemView.findViewById(R.id.marvel_heroe_name);
        }
    }
}