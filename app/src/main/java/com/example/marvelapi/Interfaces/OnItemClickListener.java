package com.example.marvelapi.Interfaces;

import com.example.marvelapi.Network.DTO.MarvelHero;

public interface OnItemClickListener {
    void onItemClick(MarvelHero item);
}
