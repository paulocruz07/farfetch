package com.example.marvelapi.Network.DTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ThumbnailDto implements Serializable {
    @SerializedName("extension")
    private String extension;
    @SerializedName("path")
    private String path;

    public ThumbnailDto(String extension, String path) {
        this.extension = extension;
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
