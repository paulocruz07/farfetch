package com.example.marvelapi.Network;

import com.example.marvelapi.Network.DTO.ResponseMarvelHeroes;
import com.example.marvelapi.Network.DTO.ResponseNameDescription;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface CommunicationInterface {

    @GET("v1/public/characters")
    Call<ResponseMarvelHeroes> getAllMarvelHeroes(@QueryMap Map<String, String> params);

    @GET("v1/public/characters/{id}/comics")
    Call<ResponseNameDescription> getComicsByCharacter(@Path("id") int groupId, @QueryMap Map<String, String> options);

    @GET("v1/public/characters/{id}/events")
    Call<ResponseNameDescription> getEventsByCharacter(@Path("id") int groupId, @QueryMap Map<String, String> options);

    @GET("v1/public/characters/{id}/series")
    Call<ResponseNameDescription> getSeriesByCharacter(@Path("id") int groupId, @QueryMap Map<String, String> options);

    @GET("v1/public/characters/{id}/stories")
    Call<ResponseNameDescription> getStoriesByCharacter(@Path("id") int groupId, @QueryMap Map<String, String> options);
}
