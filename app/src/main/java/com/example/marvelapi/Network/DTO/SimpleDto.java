package com.example.marvelapi.Network.DTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SimpleDto implements Serializable {
    @SerializedName("description")
    private String description;
    @SerializedName("title")
    private String title;

    public SimpleDto(String description, String title) {
        this.description = description;
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
