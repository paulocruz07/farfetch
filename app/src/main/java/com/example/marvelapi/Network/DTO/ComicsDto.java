package com.example.marvelapi.Network.DTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ComicsDto implements Serializable {
    @SerializedName("returned")
    private int returned;
    @SerializedName("items")
    private List<ItemsDto> items;
    @SerializedName("collectionURI")
    private String collectionURI;
    @SerializedName("available")
    private int available;

    public ComicsDto(int returned, List<ItemsDto> items, String collectionURI, int available) {
        this.returned = returned;
        this.items = items;
        this.collectionURI = collectionURI;
        this.available = available;
    }

    public int getReturned() {
        return returned;
    }

    public void setReturned(int returned) {
        this.returned = returned;
    }

    public List<ItemsDto> getItems() {
        return items;
    }

    public void setItems(List<ItemsDto> items) {
        this.items = items;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public void setCollectionURI(String collectionURI) {
        this.collectionURI = collectionURI;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }
}
