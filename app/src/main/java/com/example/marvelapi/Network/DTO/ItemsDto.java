package com.example.marvelapi.Network.DTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemsDto implements Serializable {
    @SerializedName("type")
    private String type;
    @SerializedName("name")
    private String name;
    @SerializedName("resourceURI")
    private String resourceURI;

    public ItemsDto(String type, String name, String resourceURI) {
        this.type = type;
        this.name = name;
        this.resourceURI = resourceURI;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }
}
