package com.example.marvelapi.Network.Models;

import com.example.marvelapi.Network.CommunicationInterface;
import com.example.marvelapi.Network.DTO.ResponseNameDescription;
import com.example.marvelapi.Network.ServicesURL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StoriesByCharacterModel implements Callback<ResponseNameDescription> {

    private StoriesByCharacterModel.StoriesByCharacterListener listener;


    public StoriesByCharacterModel(StoriesByCharacterModel.StoriesByCharacterListener listener) {
        this.listener = listener;
    }


    public void start(String timeStamp, String hash, int character_id) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ServicesURL.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        CommunicationInterface communicationInterface = retrofit.create(CommunicationInterface.class);

        Map<String, String> map = new HashMap<>();
        map.put("ts", timeStamp);
        map.put("apikey", ServicesURL.API_KEY);
        map.put("hash", hash);
        map.put("limit", "3");
        map.put("orderBy", "id");


        Call<ResponseNameDescription> call = communicationInterface.getStoriesByCharacter(character_id, map);
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<ResponseNameDescription> call, Response<ResponseNameDescription> response) {
        if (response.isSuccessful()) {
            listener.onStoriesByCharacterListSuccess(response.body());
        } else {
            listener.onStoriesByCharacterListFailed("");
        }
    }

    @Override
    public void onFailure(Call<ResponseNameDescription> call, Throwable t) {
        listener.onStoriesByCharacterListFailed(t.getMessage());
    }


    public interface StoriesByCharacterListener {
        void onStoriesByCharacterListSuccess(ResponseNameDescription response);

        void onStoriesByCharacterListFailed(String errorMessage);
    }
}




