package com.example.marvelapi.Network.DTO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResponseMarvelHeroes implements Serializable {

    @SerializedName("data")
    private Data data;
    @SerializedName("status")
    private String status;
    @SerializedName("code")
    private int code;

    public ResponseMarvelHeroes(Data data, String status, int code) {
        this.data = data;
        this.status = status;
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public class Data implements Serializable {
        @SerializedName("results")
        private List<MarvelHero> results;
        @SerializedName("count")
        private int count;
        @SerializedName("total")
        private int total;
        @SerializedName("limit")
        private int limit;
        @SerializedName("offset")
        private int offset;

        public Data(List<MarvelHero> results, int count, int total, int limit, int offset) {
            this.results = results;
            this.count = count;
            this.total = total;
            this.limit = limit;
            this.offset = offset;
        }

        public List<MarvelHero> getResults() {
            return results;
        }

        public void setResults(List<MarvelHero> results) {
            this.results = results;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }
    }
}
