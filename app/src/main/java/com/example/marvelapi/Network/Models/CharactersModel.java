package com.example.marvelapi.Network.Models;

import com.example.marvelapi.Network.CommunicationInterface;
import com.example.marvelapi.Network.DTO.ResponseMarvelHeroes;
import com.example.marvelapi.Network.ServicesURL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CharactersModel implements Callback<ResponseMarvelHeroes> {

    private CharactersModel.CharactersListener listener;


    public CharactersModel(CharactersModel.CharactersListener listener) {
        this.listener = listener;
    }


    public void start(String timeStamp, String hash, int offset, String name) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ServicesURL.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        CommunicationInterface communicationInterface = retrofit.create(CommunicationInterface.class);

        Map<String, String> map = new HashMap<>();
        map.put("ts", timeStamp);
        map.put("apikey", ServicesURL.API_KEY);
        map.put("hash", hash);
        map.put("offset", "" + offset);
        if (!name.isEmpty())
            map.put("name", name);


        Call<ResponseMarvelHeroes> call = communicationInterface.getAllMarvelHeroes(map);
        call.enqueue(this);

    }

    @Override
    public void onResponse(Call<ResponseMarvelHeroes> call, Response<ResponseMarvelHeroes> response) {
        if (response.isSuccessful()) {
            listener.onCharactersListSuccess(response.body());
        } else {
            listener.onCharactersListFailed("");
        }
    }

    @Override
    public void onFailure(Call<ResponseMarvelHeroes> call, Throwable t) {
        listener.onCharactersListFailed(t.getMessage());
    }


    public interface CharactersListener {
        void onCharactersListSuccess(ResponseMarvelHeroes response);

        void onCharactersListFailed(String errorMessage);
    }
}

