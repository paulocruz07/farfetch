package com.example.marvelapi.Network.DTO;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MarvelHero implements Serializable {
    @SerializedName("urls")
    private List<UrlDto> urls;
    @SerializedName("events")
    private EventsDto events;
    @SerializedName("stories")
    private StoriesDto stories;
    @SerializedName("series")
    private SeriesDto series;
    @SerializedName("comics")
    private ComicsDto comics;
    @SerializedName("resourceURI")
    private String resourceURI;
    @SerializedName("thumbnail")
    private ThumbnailDto thumbnail;
    @SerializedName("modified")
    private String modified;
    @SerializedName("description")
    private String description;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private int id;

    private boolean isFavourite = false;

    public MarvelHero(List<UrlDto> urls, EventsDto events, StoriesDto stories, SeriesDto series, ComicsDto comics, String resourceURI, ThumbnailDto thumbnail, String modified, String description, String name, int id) {
        this.urls = urls;
        this.events = events;
        this.stories = stories;
        this.series = series;
        this.comics = comics;
        this.resourceURI = resourceURI;
        this.thumbnail = thumbnail;
        this.modified = modified;
        this.description = description;
        this.name = name;
        this.id = id;
    }

    public List<UrlDto> getUrls() {
        return urls;
    }

    public void setUrls(List<UrlDto> urls) {
        this.urls = urls;
    }

    public EventsDto getEvents() {
        return events;
    }

    public void setEvents(EventsDto events) {
        this.events = events;
    }

    public StoriesDto getStories() {
        return stories;
    }

    public void setStories(StoriesDto stories) {
        this.stories = stories;
    }

    public SeriesDto getSeries() {
        return series;
    }

    public void setSeries(SeriesDto series) {
        this.series = series;
    }

    public ComicsDto getComics() {
        return comics;
    }

    public void setComics(ComicsDto comics) {
        this.comics = comics;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public ThumbnailDto getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailDto thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof MarvelHero)) {
            return false;
        }

        return this.getId() == ((MarvelHero) obj).getId();
    }
}
