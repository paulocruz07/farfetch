package com.example.marvelapi.Controllers;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvelapi.Adapters.PagesAdapter;
import com.example.marvelapi.DatabaseConnection.MarvelHeroesDbConnection;
import com.example.marvelapi.Interfaces.OnItemClickListener;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListHeroesFragment extends Fragment implements OnItemClickListener {

    private ScrollView rootView;

    @BindView(R.id.pages_recycler_view)
    public RecyclerView pages_recycler_view;

    private PagesAdapter adapter;
    private MarvelHeroesDbConnection marvelHeroesDbConnection;

    public static final String HERO_KEY = "hero_key";
    public static final String DB_CONNECTION = "db_connection";


    public static ListHeroesFragment newInstance() {
        ListHeroesFragment frag = new ListHeroesFragment();

        Bundle args = new Bundle();
        frag.setArguments(args);

        return frag;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ScrollView) inflater
                .inflate(R.layout.fragment_main, container, false);

        ButterKnife.bind(this, rootView);

        marvelHeroesDbConnection = ((MainActivity) getActivity()).openDatabase();

        ArrayList<MarvelHero> favourites_list = marvelHeroesDbConnection.getDataFromDB();

        adapter = new PagesAdapter(getActivity(), 2, this, favourites_list);
        pages_recycler_view.setAdapter(adapter);
        pages_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));

        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(pages_recycler_view);

        return rootView;

    }

    @Override
    public void onItemClick(MarvelHero item) {
        // Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(getActivity(), HeroDetailsActivity.class);
            intent.putExtra(HERO_KEY, item);
            startActivityForResult(intent, 101, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
        } else {
            Intent intent = new Intent(getActivity(), HeroDetailsActivity.class);
            intent.putExtra(HERO_KEY, item);
            startActivityForResult(intent, 101);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == 10) {
                ArrayList<MarvelHero> favourites_list = marvelHeroesDbConnection.getDataFromDB();
                adapter.setFavouritesList(favourites_list);
            }
        }
    }
}
