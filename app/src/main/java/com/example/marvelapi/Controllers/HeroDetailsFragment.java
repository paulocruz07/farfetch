package com.example.marvelapi.Controllers;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.marvelapi.Adapters.SimpleAdapter;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.Network.DTO.ResponseNameDescription;
import com.example.marvelapi.Network.DTO.SimpleDto;
import com.example.marvelapi.Network.Models.ComicsByCharacterModel;
import com.example.marvelapi.Network.Models.EventsByCharacterModel;
import com.example.marvelapi.Network.Models.SeriesByCharacterModel;
import com.example.marvelapi.Network.Models.StoriesByCharacterModel;
import com.example.marvelapi.Network.ServicesURL;
import com.example.marvelapi.R;
import com.example.marvelapi.Utils.Configs;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HeroDetailsFragment extends Fragment implements ComicsByCharacterModel.ComicsByCharacterListener,
        EventsByCharacterModel.EventsByCharacterListener, StoriesByCharacterModel.StoriesByCharacterListener,
        SeriesByCharacterModel.SeriesByCharacterListener {


    private static final String HERO_KEY = "hero_key";

    private NestedScrollView rootView;

    @BindView(R.id.heroe_image)
    public ImageView heroe_image;

    @BindView(R.id.hero_name)
    public TextView hero_name;

    @BindView(R.id.hero_description)
    public TextView hero_description;

    @BindView(R.id.hero_resource)
    public TextView hero_resource;

    @BindView(R.id.no_data_comics)
    public TextView no_data_comics;

    @BindView(R.id.no_data_events)
    public TextView no_data_events;

    @BindView(R.id.no_data_series)
    public TextView no_data_series;

    @BindView(R.id.no_data_stories)
    public TextView no_data_stories;

    @BindView(R.id.comics_recycler_view)
    public RecyclerView comics_recycler_view;

    @BindView(R.id.events_recycler_view)
    public RecyclerView events_recycler_view;

    @BindView(R.id.stories_recycler_view)
    public RecyclerView stories_recycler_view;

    @BindView(R.id.series_recycler_view)
    public RecyclerView series_recycler_view;


    private MarvelHero marvelHero;
    private ProgressDialog dialog;
    private int number_completed = 0;


    public static HeroDetailsFragment newInstance(MarvelHero hero) {
        HeroDetailsFragment frag = new HeroDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(HERO_KEY, hero);
        frag.setArguments(args);

        return frag;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (getArguments() != null) {
            this.marvelHero = (MarvelHero) getArguments().get(HERO_KEY);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (NestedScrollView) inflater
                .inflate(R.layout.heroe_detail_fragment, container, false);

        ButterKnife.bind(this, rootView);


        String name, resource;
        name = String.format(getResources().getString(R.string.hero_name), (marvelHero.getName().isEmpty()) ? "-" : marvelHero.getName());
        resource = String.format(getResources().getString(R.string.hero_resource_uri), (marvelHero.getResourceURI().isEmpty()) ? "-" : marvelHero.getResourceURI());

        if (marvelHero != null) {
            hero_name.setText(name);
            hero_description.setText((marvelHero.getDescription().isEmpty()) ? "-" : marvelHero.getDescription());
            hero_resource.setText(resource);
            Glide.with(this).load((marvelHero.getThumbnail().getPath() + "/standard_amazing." + marvelHero.getThumbnail().getExtension()).replace("http", "https")).into(heroe_image);
            dialog = ProgressDialog.show(getActivity(), "",
                    "Loading. Please wait...", true);
            callGetComicsByCharacter(marvelHero.getId());
            callGetEventsByCharacter(marvelHero.getId());
            callGetSeriesByCharacter(marvelHero.getId());
            callGetStorieByCharacter(marvelHero.getId());
        }

        return rootView;
    }

    private void callGetComicsByCharacter(int id) {
        String ts = Configs.getTimeStamp();
        String string_to_be_converted_to_MD5 = ts + Configs.getPrivateKey(getActivity()) + ServicesURL.API_KEY;
        String MD5_Hash_String = Configs.md5(string_to_be_converted_to_MD5);

        ComicsByCharacterModel charactersModel = new ComicsByCharacterModel(this);
        charactersModel.start(ts, MD5_Hash_String, id);
    }

    @Override
    public void onComicsByCharacterListSuccess(ResponseNameDescription response) {
        if (response.getData() != null && response.getData().getResults() != null && !response.getData().getResults().isEmpty()) {
            no_data_comics.setVisibility(View.GONE);
            comics_recycler_view.setVisibility(View.VISIBLE);
            comics_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            comics_recycler_view.setNestedScrollingEnabled(false);
            SimpleAdapter adapter = new SimpleAdapter(getActivity(), (ArrayList<SimpleDto>) response.getData().getResults());
            comics_recycler_view.setAdapter(adapter);
        } else {
            no_data_comics.setVisibility(View.VISIBLE);
            comics_recycler_view.setVisibility(View.GONE);
        }
        incrementSuccess();
    }

    @Override
    public void onComicsByCharacterListFailed(String errorMessage) {
        incrementSuccess();
        no_data_comics.setVisibility(View.VISIBLE);
    }

    private void callGetEventsByCharacter(int id) {
        String ts = Configs.getTimeStamp();
        String string_to_be_converted_to_MD5 = ts + Configs.getPrivateKey(getActivity()) + ServicesURL.API_KEY;
        String MD5_Hash_String = Configs.md5(string_to_be_converted_to_MD5);

        EventsByCharacterModel charactersModel = new EventsByCharacterModel(this);
        charactersModel.start(ts, MD5_Hash_String, id);
    }


    @Override
    public void onEventsByCharacterListSuccess(ResponseNameDescription response) {
        if (response.getData() != null && response.getData().getResults() != null && !response.getData().getResults().isEmpty()) {
            no_data_events.setVisibility(View.GONE);
            comics_recycler_view.setVisibility(View.VISIBLE);
            events_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            SimpleAdapter adapter = new SimpleAdapter(getActivity(), (ArrayList<SimpleDto>) response.getData().getResults());
            comics_recycler_view.setNestedScrollingEnabled(false);
            events_recycler_view.setAdapter(adapter);
        } else {
            no_data_events.setVisibility(View.VISIBLE);
            comics_recycler_view.setVisibility(View.GONE);
        }
        incrementSuccess();
    }

    @Override
    public void onEventsByCharacterListFailed(String errorMessage) {
        incrementSuccess();
        no_data_events.setVisibility(View.VISIBLE);
        comics_recycler_view.setVisibility(View.GONE);
    }

    private void callGetSeriesByCharacter(int id) {
        String ts = Configs.getTimeStamp();
        String string_to_be_converted_to_MD5 = ts + Configs.getPrivateKey(getActivity()) + ServicesURL.API_KEY;
        String MD5_Hash_String = Configs.md5(string_to_be_converted_to_MD5);

        SeriesByCharacterModel charactersModel = new SeriesByCharacterModel(this);
        charactersModel.start(ts, MD5_Hash_String, id);
    }

    @Override
    public void onSeriesByCharacterListSuccess(ResponseNameDescription response) {
        if (response.getData() != null && response.getData().getResults() != null && !response.getData().getResults().isEmpty()) {
            no_data_series.setVisibility(View.GONE);
            comics_recycler_view.setVisibility(View.VISIBLE);
            series_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            SimpleAdapter adapter = new SimpleAdapter(getActivity(), (ArrayList<SimpleDto>) response.getData().getResults());
            comics_recycler_view.setNestedScrollingEnabled(false);
            series_recycler_view.setAdapter(adapter);
        } else {
            no_data_series.setVisibility(View.VISIBLE);
            comics_recycler_view.setVisibility(View.GONE);
        }
        incrementSuccess();
    }

    @Override
    public void onSeriesByCharacterListFailed(String errorMessage) {
        incrementSuccess();
        no_data_series.setVisibility(View.VISIBLE);
        comics_recycler_view.setVisibility(View.GONE);
    }

    private void callGetStorieByCharacter(int id) {
        String ts = Configs.getTimeStamp();
        String string_to_be_converted_to_MD5 = ts + Configs.getPrivateKey(getActivity()) + ServicesURL.API_KEY;
        String MD5_Hash_String = Configs.md5(string_to_be_converted_to_MD5);

        StoriesByCharacterModel charactersModel = new StoriesByCharacterModel(this);
        charactersModel.start(ts, MD5_Hash_String, id);
    }

    @Override
    public void onStoriesByCharacterListSuccess(ResponseNameDescription response) {
        if (response.getData() != null && response.getData().getResults() != null && !response.getData().getResults().isEmpty()) {
            no_data_stories.setVisibility(View.GONE);
            comics_recycler_view.setVisibility(View.VISIBLE);
            stories_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
            SimpleAdapter adapter = new SimpleAdapter(getActivity(), (ArrayList<SimpleDto>) response.getData().getResults());
            comics_recycler_view.setNestedScrollingEnabled(false);
            stories_recycler_view.setAdapter(adapter);
        } else {
            no_data_stories.setVisibility(View.VISIBLE);
            comics_recycler_view.setVisibility(View.GONE);
        }
        incrementSuccess();
    }

    @Override
    public void onStoriesByCharacterListFailed(String errorMessage) {
        incrementSuccess();
        no_data_stories.setVisibility(View.VISIBLE);
        comics_recycler_view.setVisibility(View.GONE);
    }

    private void incrementSuccess() {
        number_completed++;
        if (number_completed == 3) {
            dialog.dismiss();
        }
    }

}
