package com.example.marvelapi.Controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.R;

public class HeroDetailsActivity extends AppCompatActivity {

    private MarvelHero marvelHero;
    private boolean was_changed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {

            marvelHero = null;
            if (getIntent().getSerializableExtra(ListHeroesFragment.HERO_KEY) != null) {
                marvelHero = (MarvelHero) getIntent().getSerializableExtra(ListHeroesFragment.HERO_KEY);

            }
            configureToolbar();

            HeroDetailsFragment fragment = HeroDetailsFragment.newInstance(marvelHero);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }
    }

    private void configureToolbar() {
        TextView back_arrow = findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        back_arrow.setVisibility(View.VISIBLE);

        ImageView search_icon = findViewById(R.id.search_icon);
        search_icon.setVisibility(View.GONE);

        ImageView favourite_image = findViewById(R.id.favourite_image);
        favourite_image.setVisibility(View.VISIBLE);
        if (marvelHero != null) {
            if (marvelHero.isFavourite()) {
                favourite_image.setImageResource(R.drawable.logo);
            } else {
                favourite_image.setImageResource(R.drawable.logo_black);
            }

            favourite_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (marvelHero.isFavourite()) {
                        was_changed = true;
                        marvelHero.setFavourite(false);
                        favourite_image.setImageResource(R.drawable.logo_black);
                        MainActivity.saveFavourite(marvelHero, false);
                    } else {
                        was_changed = true;
                        marvelHero.setFavourite(true);
                        favourite_image.setImageResource(R.drawable.logo);
                        MainActivity.saveFavourite(marvelHero, true);
                    }
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        if (was_changed) {
            setResult(10);
            finish();
        } else {
            super.onBackPressed();
        }
    }

}
