package com.example.marvelapi.Controllers;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.marvelapi.DatabaseConnection.MarvelHeroesDbConnection;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.R;

public class MainActivity extends AppCompatActivity {

    private RelativeLayout root_view;
    private static MarvelHeroesDbConnection marvelHeroesDbConnection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            formatSearchIcon();
            ListHeroesFragment fragment = ListHeroesFragment.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }
    }

    public void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    public MarvelHeroesDbConnection openDatabase() {
        marvelHeroesDbConnection = new MarvelHeroesDbConnection(this);
        return marvelHeroesDbConnection;
    }

    private void formatSearchIcon() {
        ImageView search_icon = findViewById(R.id.search_icon);
        search_icon.setVisibility(View.VISIBLE);
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                SearchCharacterDialog editNameDialogFragment = SearchCharacterDialog.newInstance("Some Title");
                editNameDialogFragment.show(fm, "fragment_edit_name");
            }
        });

        ImageView favourite_image = findViewById(R.id.favourite_image);
        favourite_image.setVisibility(View.GONE);

    }

    @Override
    protected void onDestroy() {
        marvelHeroesDbConnection.closeConnection();
        super.onDestroy();
    }

    public static void saveFavourite(MarvelHero hero, boolean isToSave) {
        if (isToSave)
            marvelHeroesDbConnection.insert(hero);
        else
            marvelHeroesDbConnection.deleteData(hero);
    }

}
