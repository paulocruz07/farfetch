package com.example.marvelapi.Controllers;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.marvelapi.Adapters.HeroeListAdapter;
import com.example.marvelapi.Adapters.SearchAdapter;
import com.example.marvelapi.Adapters.SimpleAdapter;
import com.example.marvelapi.Interfaces.OnItemClickListener;
import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.Network.DTO.ResponseMarvelHeroes;
import com.example.marvelapi.Network.Models.CharactersModel;
import com.example.marvelapi.Network.ServicesURL;
import com.example.marvelapi.R;
import com.example.marvelapi.Utils.Configs;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchCharacterDialog extends DialogFragment implements CharactersModel.CharactersListener, OnItemClickListener {

    @BindView(R.id.search_edit_text)
    public EditText search_edit_text;

    @BindView(R.id.search_btn)
    public ImageView search_btn;

    @BindView(R.id.search_list)
    public RecyclerView search_list;

    @BindView(R.id.no_data)
    public TextView no_data;

    private ProgressDialog dialog;

    public SearchCharacterDialog() {
        // Empty constructor is required for DialogFragment
    }

    public static SearchCharacterDialog newInstance(String title) {
        SearchCharacterDialog frag = new SearchCharacterDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LinearLayout root_view = (LinearLayout) inflater.inflate(R.layout.search_dialog_fragment, container);

        ButterKnife.bind(this, root_view);

        return root_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!search_edit_text.getText().toString().isEmpty()) {
                    callGetMarvelHeroes(search_edit_text.getText().toString());
                }
            }
        });

    }

    private void callGetMarvelHeroes(String name) {
        dialog = ProgressDialog.show(getActivity(), "",
                "Loading. Please wait...", true);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(search_edit_text.getWindowToken(), 0);
        String ts = Configs.getTimeStamp();
        String string_to_be_converted_to_MD5 = ts + Configs.getPrivateKey(getActivity()) + ServicesURL.API_KEY;
        String MD5_Hash_String = Configs.md5(string_to_be_converted_to_MD5);

        CharactersModel charactersModel = new CharactersModel(this);
        charactersModel.start(ts, MD5_Hash_String, 0, name);
    }

    @Override
    public void onCharactersListSuccess(ResponseMarvelHeroes response) {
        if (response.getData() != null && response.getData().getResults() != null && !response.getData().getResults().isEmpty()) {
            no_data.setVisibility(View.GONE);
            search_list.setVisibility(View.VISIBLE);
            search_list.setLayoutManager(new LinearLayoutManager(getActivity()));
            search_list.setAdapter(new SearchAdapter(getActivity(), (ArrayList<MarvelHero>) response.getData().getResults(), this));
        } else {
            no_data.setVisibility(View.VISIBLE);
            search_list.setVisibility(View.GONE);
        }
        dialog.dismiss();
    }

    @Override
    public void onCharactersListFailed(String errorMessage) {
        dialog.dismiss();
        no_data.setVisibility(View.VISIBLE);
        search_list.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(MarvelHero item) {
        Intent intent = new Intent(getActivity(), HeroDetailsActivity.class);
        intent.putExtra(ListHeroesFragment.HERO_KEY, item);
        startActivity(intent);
        dismiss();
    }
}