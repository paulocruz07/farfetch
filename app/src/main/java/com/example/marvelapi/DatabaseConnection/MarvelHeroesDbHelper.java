package com.example.marvelapi.DatabaseConnection;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MarvelHeroesDbHelper extends SQLiteOpenHelper{
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MarvelFavourites.db";

    public MarvelHeroesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    private static final String SQL_CREATE_HERO_ENTRY =
            "CREATE TABLE " + MarvelHeroesContract.HeroEntry.TABLE_NAME + " (" +
                    MarvelHeroesContract.HeroEntry._ID + " INTEGER PRIMARY KEY," +
                    MarvelHeroesContract.HeroEntry.COLUMN_NAME_NAME + " TEXT," +
                    MarvelHeroesContract.HeroEntry.COLUMN_NAME_DESCRIPTION + " TEXT," +
                    MarvelHeroesContract.HeroEntry.COLUMN_NAME_MODIFIED + " TEXT," +
                    MarvelHeroesContract.HeroEntry.COLUMN_NAME_RESOURCE_URI + " TEXT);";

    private static final String SQL_CREATE_THUMBNAIL_ENTRY =
            "CREATE TABLE " + MarvelHeroesContract.ThumbnailEntry.TABLE_NAME + " (" +
                    MarvelHeroesContract.ThumbnailEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_PATH + " TEXT NOT NULL, " +
                    MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_HERO_ID + " INTEGER NOT NULL, " +
                    MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_EXTENSION + " TEXT NOT NULL, " +

                    // Set up the column as a foreign key to table.
                    " FOREIGN KEY (" + MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_HERO_ID + ") REFERENCES " +
                    MarvelHeroesContract.HeroEntry._ID + " (" + MarvelHeroesContract.ThumbnailEntry._ID + ") )"
                    + ";";


    private static final String SQL_DELETE_HEROE_ENTRY =
            "DROP TABLE IF EXISTS " + MarvelHeroesContract.HeroEntry.TABLE_NAME;

    private static final String SQL_DELETE_THUMBNAIL_ENTRY =
            "DROP TABLE IF EXISTS " + MarvelHeroesContract.ThumbnailEntry.TABLE_NAME;


    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_HERO_ENTRY);
        db.execSQL(SQL_CREATE_THUMBNAIL_ENTRY);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_HEROE_ENTRY);
        db.execSQL(SQL_DELETE_THUMBNAIL_ENTRY);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}

