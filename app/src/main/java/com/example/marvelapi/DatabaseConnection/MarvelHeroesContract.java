package com.example.marvelapi.DatabaseConnection;

import android.provider.BaseColumns;

public class MarvelHeroesContract {

    private MarvelHeroesContract() {
    }

    /* Inner class that defines the table contents */
    public static class HeroEntry implements BaseColumns {
        public static final String TABLE_NAME = "hero_favourites";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_MODIFIED = "modified";
        public static final String COLUMN_NAME_RESOURCE_URI = "resourceURI";
    }

    public static class ThumbnailEntry implements BaseColumns {
        public static final String TABLE_NAME = "hero_thumbnail";
        public static final String COLUMN_NAME_THUMBNAIL_EXTENSION = "extension";
        public static final String COLUMN_NAME_THUMBNAIL_PATH = "path";
        public static final String COLUMN_NAME_HERO_ID = "hero_id";

    }

}
