package com.example.marvelapi.DatabaseConnection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.example.marvelapi.Network.DTO.MarvelHero;
import com.example.marvelapi.Network.DTO.ThumbnailDto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MarvelHeroesDbConnection{

    private MarvelHeroesDbHelper mOpenHelper;

    public MarvelHeroesDbConnection(Context context) {
        mOpenHelper = new MarvelHeroesDbHelper(context);
    }

    public void insert(MarvelHero marvelHero) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(MarvelHeroesContract.HeroEntry.COLUMN_NAME_NAME, marvelHero.getName());
        values.put(MarvelHeroesContract.HeroEntry.COLUMN_NAME_DESCRIPTION, marvelHero.getDescription());
        values.put(MarvelHeroesContract.HeroEntry._ID, marvelHero.getId());
        values.put(MarvelHeroesContract.HeroEntry.COLUMN_NAME_MODIFIED, marvelHero.getModified());
        values.put(MarvelHeroesContract.HeroEntry.COLUMN_NAME_RESOURCE_URI, marvelHero.getResourceURI());
        insertThumbnail(marvelHero.getThumbnail().getExtension(), marvelHero.getThumbnail().getPath(), marvelHero.getId(), db);

        long row = db.insert(MarvelHeroesContract.HeroEntry.TABLE_NAME, null, values);
    }

    private long insertThumbnail(String extension, String path, int id, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_EXTENSION, extension);
        values.put(MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_PATH, path);
        values.put(MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_HERO_ID, id);
        return db.insert(MarvelHeroesContract.ThumbnailEntry.TABLE_NAME, null, values);
    }


    public ArrayList<MarvelHero> getDataFromDB() {
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        String[] projection = {
                MarvelHeroesContract.HeroEntry.TABLE_NAME + "." + MarvelHeroesContract.HeroEntry._ID,
                MarvelHeroesContract.HeroEntry.COLUMN_NAME_NAME,
                MarvelHeroesContract.HeroEntry.COLUMN_NAME_DESCRIPTION,
                MarvelHeroesContract.HeroEntry.COLUMN_NAME_MODIFIED,
                MarvelHeroesContract.HeroEntry.COLUMN_NAME_RESOURCE_URI,
                MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_EXTENSION,
                MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_PATH,
                MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_HERO_ID
        };

        String selection = MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_HERO_ID + " = " + MarvelHeroesContract.HeroEntry.TABLE_NAME + "." + MarvelHeroesContract.HeroEntry._ID;


        Cursor cursor = db.query(
                MarvelHeroesContract.HeroEntry.TABLE_NAME + "," + MarvelHeroesContract.ThumbnailEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                MarvelHeroesContract.HeroEntry.COLUMN_NAME_NAME               // The sort order
        );

        ArrayList<MarvelHero> itemIds = new ArrayList<>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.HeroEntry._ID));
            String name = cursor.getString(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.HeroEntry.COLUMN_NAME_NAME));
            String resourceURI = cursor.getString(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.HeroEntry.COLUMN_NAME_RESOURCE_URI));
            String descritpion = cursor.getString(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.HeroEntry.COLUMN_NAME_DESCRIPTION));
            String modified = cursor.getString(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.HeroEntry.COLUMN_NAME_MODIFIED));
            String extension = cursor.getString(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_EXTENSION));
            String path = cursor.getString(
                    cursor.getColumnIndexOrThrow(MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_THUMBNAIL_PATH));

            MarvelHero marvelHero = new MarvelHero(null, null, null, null, null, resourceURI, new ThumbnailDto(extension, path), modified, descritpion, name, (int) itemId);

            itemIds.add(marvelHero);
        }
        cursor.close();
        return itemIds;
    }


    public void deleteData(MarvelHero marvelHero) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();


        // Define 'where' part of query.
        String selection2 = MarvelHeroesContract.ThumbnailEntry.COLUMN_NAME_HERO_ID + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs2 = {"" + marvelHero.getId()};
        db.delete(MarvelHeroesContract.ThumbnailEntry.TABLE_NAME, selection2, selectionArgs2);

        // Define 'where' part of query.
        String selection = MarvelHeroesContract.HeroEntry._ID + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {"" + marvelHero.getId()};
        // Issue SQL statement.
        db.delete(MarvelHeroesContract.HeroEntry.TABLE_NAME, selection, selectionArgs);

    }


    public void closeConnection() {
        mOpenHelper.close();
    }

}
