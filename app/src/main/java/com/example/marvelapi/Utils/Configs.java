package com.example.marvelapi.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.example.marvelapi.BuildConfig;
import com.example.marvelapi.Network.ServicesURL;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Configs {

    public static String getPrivateKey(Context context) {
        return BuildConfig.MarvelSecAPIKey;
    }

    public static String md5(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(s.getBytes());
            byte[] a = digest.digest();
            int len = a.length;
            StringBuilder sb = new StringBuilder(len << 1);
            for (int i = 0; i < len; i++) {
                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
                sb.append(Character.forDigit(a[i] & 0x0f, 16));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        return ts;
    }


}
